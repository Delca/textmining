#ifndef __NODE__HH__
#define __NODE__HH__

#include <iostream>
#include <vector>
#include "fileNode.hh"
#include "convertTable.hh"

typedef struct Node {
  char c; /// the character represented by the node in the trie
  int data; /// the frequency of the word if the current letter is a terminal one. 0 otherwise

  std::vector<Node*> children; /// the children nodes

  static int nodeNum;

  void printNode(int indent);
  /**
   * Retruns how many nodes are under this node. Used as an offset to the next sibling node to navigate through the serialized trie.
   */
  int getChildrenNum();

  /**
   * Convert the Node to the serialized form 
   */
  FileNode asFileNode();
  /**
   * Convert the Node to the serialized form and display it 
   */
  FileNode asFileNode(bool debug);

  /**
   * Writes the node to a std::ostream in serialized form
   */
  void writeToStream(std::ostream& stream);
  /**
   * Read serialized data from a std::istream and unserialize
   * it.
   */
  static Node* fromStream(std::istream& stream);

  /**
   * Default constructor
   */
  Node() : c(0), data(0), children(0) {++nodeNum;}
  Node(char c, int d) : c(c), data(d), children(0){++nodeNum;}
  Node(char c, int d, std::vector<Node*> v) : c(c), data(d), children(v){++nodeNum;}

  ~Node();

} Node;

#endif
