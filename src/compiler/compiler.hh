#ifndef __COMPILER__HH__
#define __COMPILER__HH__

#include <string>
#include <iostream>
#include <vector>
#include <fstream>
#include <cstdint>
#include "convertTable.hh"
#include "node.hh"
#include "fileNode.hh"

/**
 *  The Compiler class which compile a dictionary in a binary file
 */

class Compiler {

private:

  Node* root; ///< root of the tree
  /**
   * Adds the given word to the trie from the level of the nodes given in the std::vector
   *
   */
  void insertWord(std::vector<Node*>& vector, std::string word, int data);

public:

  /**
   *  Constructor of the Compiler class
   *  Create a root node
   */
  Compiler();

  /**
   *  Destructor of the App class
   */  
  ~Compiler();

  /**
   *  Read the dictionary file and construct the trie
   *  @param filename filename of the dictionary file 
   */
  void readFile(std::string filename);

  /**
   *  Write the binary file
   *  @param filename filename of the binary file 
   */
  void writeDict(std::string filename);
  /**
   * Method used to serialize the node retrived from
   * the readFromFile method.
   */
  static void writeDict(std::string filename, Node* root);
  /**
   * Read the whole data structure from the serialized file.
   * Used to check that serialization in cohrent and deterministic
   */
  static Node* readFromFile(std::string filename);

  /**
   * Print the trie, one node per line.
   */
  void printMe();
  
};

#endif
