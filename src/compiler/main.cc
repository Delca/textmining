#include "compiler.hh"
#include <iostream>

int main(int argc, char** argv) {
  Compiler* c = new Compiler();

  if (argc != 3) {
    // TODO: print usage
    printf("LOL NOPE\n");
    return 1;
  }

  c->readFile(argv[1]);
  c->writeDict(argv[2]);

  delete c;

  return 0;
}
