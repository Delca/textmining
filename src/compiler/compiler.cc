#include "compiler.hh"

int Node::nodeNum = 0;

Compiler::Compiler() {
  root = new Node('n', 0);
}

void Compiler::readFile(std::string filename) {
  std::string currLine = "";
  std::string currFreq = "";
  std::ifstream fileStream(filename);

  /// This part is not optimised, but the app is the important part
  while (std::getline(fileStream, currLine, '\t')) {
    std::getline(fileStream, currFreq);
    insertWord(root->children, currLine, std::atoi(currFreq.c_str()));
  }

  fileStream.close();
}

void Compiler::printMe() {
  for (auto node = root->children.begin(); node != root->children.end(); ++node) {
    (*node)->printNode(0);
  }
}

void Compiler::insertWord(std::vector<Node*>& vector, std::string word, int data) {
  std::vector<Node*>* currVect = &vector;

  int i = -1;
  /// For each letter in the word ...
  while(++i < word.size()) {
    char wChar = word[i];
    Node* n = nullptr;

    for (auto node = currVect->begin(); node != currVect->end(); ++node) {
      /// If the letter is already represented in a node ...
      if ((*node)->c == wChar) {
	n = *node;
	/// ... then mark the node as the current one and continue with the following letter
	currVect = &((*node)->children);
	wChar = 0;
	break;
      }
    }

    /// If there is no node containing the letter, create it
    if (wChar != 0) {
      n = new Node(wChar, (i==word.size()-1?data:0));
      currVect->push_back(n);
      currVect = &(n->children);
    }
    else 
      // If we reached the last letter, insert the data at the current node
      if (i == word.size()-1) {
      n->data = data;
    }

  }

}

void Compiler::writeDict(std::string filename) {
  Compiler::writeDict(filename, root);
}

void Compiler::writeDict(std::string filename, Node* root) {
  std::ofstream file(filename);

  if (file.is_open()) {
    root->writeToStream(file);
    file.close();
  }
  else {
    std::cerr << "Failed to write file " << filename << std::endl;
  }

}

Node* Compiler::readFromFile(std::string filename) {
  std::ifstream input(filename);
  
  if (input.is_open()) {
    Node* node = Node::fromStream(input);
    return node;
  }
  else {
    std::cerr << "Could not read from file " << filename << std::endl;
  }

  return nullptr;
}

Compiler::~Compiler() {
  for (auto node = root->children.begin(); node != root->children.end(); ++node) {
    delete (*node);
  }
}
