#ifndef __FILENODE__HH__
#define __FILENODE__HH__

/**
 * A struct to represent a node in the serialized trie format
 */
typedef struct {
  uint32_t data; /** the frequency of the word which final letter is the current node. 0 if the letter does not end a word */
  uint32_t childrenNum; /** how many nodes are under this one, which is the offset to the next brother node (if any) */
  unsigned char character; /** the character of the node */
  unsigned char directChildrenNum; /** how many direct children this node has   */
} __attribute__((packed)) FileNode;

#endif

