#include "node.hh"

void Node::printNode(int indent = 0) {
  std::cout << std::string("").append(indent, ' ') << c << std::endl;
  ++indent;
  for (auto node = children.begin(); node != children.end(); ++node) {
    (*node)->printNode(indent);
  }
}

Node::~Node() {
  for (auto node = children.begin(); node != children.end(); ++node) {
    delete (*node);
  }
}

int Node::getChildrenNum() {
  int total = 1; // the node itself

  for (auto node = children.begin(); node != children.end(); ++node) {
    total += (*node)->getChildrenNum();
  }

  return total;
}

FileNode Node::asFileNode(bool debug) {
  FileNode ans = asFileNode();

  if (debug) {
    std::cout << "NODE ---- FILE" << std::endl;
    std::cout << "data " << data << " " << ans.data << std::endl; 
    std::cout << "char " << c << " " << fromDataFormat[ans.character] << std::endl; 
    std::cout << "drCN " << children.size() << " " << (int) ans.directChildrenNum << std::endl; 
    std::cout << "cNum " << getChildrenNum() << " " << ans.childrenNum << std::endl << std::endl;
  }

  return ans;
}

FileNode Node::asFileNode() {
  FileNode ans;

  ans.data = data;
  ans.character = toDataFormat[c];
  ans.directChildrenNum = (unsigned int)children.size();
  ans.childrenNum = (unsigned int) getChildrenNum();

  return ans;
}

void Node::writeToStream(std::ostream& stream) {
  FileNode fileData = this->asFileNode();
  stream.write((char*) &fileData, sizeof(FileNode));

  for (auto ite = children.begin(); ite != children.end(); ++ite) {
    (*ite)->writeToStream(stream);
  }

}

Node* Node::fromStream(std::istream& stream) {
  FileNode* dataNode = (FileNode*) malloc(sizeof(FileNode));

  stream.read((char*) dataNode, sizeof(FileNode) );

  Node* node = new Node(fromDataFormat[dataNode->character], dataNode->data);

  for (int i = 0; i < (int)dataNode->directChildrenNum; ++i) {
    node->children.push_back(Node::fromStream(stream));
  }


    return node;
}

