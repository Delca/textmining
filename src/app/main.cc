#include "../compiler/compiler.hh"
#include "app.hh"
#include <stdlib.h>

int main(int argc, char *argv[])
{
    // We will not handle strings with more than 256 characters.
    // There is two spaces in a line
    // We will not handle differences greater than 999 (which is already way too much)
    // "approx" is 6 characters long
    // And we give a little more space for '\0'
    char buf[256 + 2 + 3 + 6 + 1] = {0};
    App a = App();
    FileNode* root = a.openDict(argv[1]);
    int distance = 0;
    while (scanf("approx %d %256[^\n]%*c", &distance, buf) == 2) {
      a.searchWordUNREC(root, buf, distance);
      a.writeJson();
      *buf = '\0';
      distance = -1;
    }
}
