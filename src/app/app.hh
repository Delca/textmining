#ifndef __APP__HH__
#define __APP__HH__

#include <iostream>
#include <vector>
#include <algorithm>
#include <stdio.h>
#include <string.h>
#include <utility>      
#include <string> 
#include "../compiler/compiler.hh"
#include "../compiler/convertTable.hh"
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h>

static unsigned char MAX_TREE_DEPTH = 176 + 3;

/**
 *	The app class which search a word in a dictionary
 */


class App {

public:

	/**
	 *	Constructor of the App class
	 *	Clear all the vectors 
	 */
	App();

	/**
	 *	Destructor of the App class
	 */
	virtual ~App();

	/**
	 *	Write in the output in format JSON the result for one word
	 */
	void writeJson();

	/**
	 *	Search all the words in the given distance in a dictionary
	 *	@param root the root node of the trie
	 *	@param word the original word
	 *	@param distance the number of mistakes
	 */
    void searchWordUNREC(FileNode* root, char* word, int distance);

    /**
     *	Compute the "ind" line of the Damerau Levenshtein distance
     *	@param ind depth of the tree
     *	@param letter letter of the "ind" line
     *	@param prev_letter letter of the "ind - 1" line
     *	@param dist array which stock the Damerau Levenshtein distance
     */
  void computeDistanceUNREC(int ind, char letter, char prev_letter, char* word, int size_word, unsigned char** dist);

  	/**
  	 *	return the minimum distance of a line in an array
  	 *	@param dist array which stock the Damerau Levenshtein distance
  	 *	@param the size of the line in the array
  	 *	@param the ind of the line
  	 *	@return the minimum distance
  	 */
  	unsigned char minimumDistanceUNREC(unsigned char** dist, unsigned char size, unsigned char ind);

  static FileNode* openDict(std::string filename);

private:

	std::pair<int, std::string> dist0;	 ///< std::pair which stocks word of distance 0
	std::vector<std::pair<int, std::string>> dist1;	///< std::vector which stocks all the words of distance 1
	std::vector<std::pair<int, std::string>> dist2;	///< std::vector which stocks all the words of distance 2
	std::vector<std::pair<int, std::string>> dist3; ///< std::vector which stocks all the words of distance 3 and plus
};



#endif
