#include "app.hh"

App::App()
{
  dist0 = std::make_pair(0, "");
  dist1.clear();
  dist2.clear();
  dist3.clear();
}

App::~App()
{

}

/**
 *	Compare function to sort all the distances
 */
bool myCompareFunction(std::pair<int,std::string> i, std::pair<int,std::string> j)
{
	if (i.first != j.first)
        return i.first > j.first;

    return i.second < j.second;
}

void App::writeJson()
{

	sort(dist1.begin(), dist1.end(), myCompareFunction);
	sort(dist2.begin(), dist2.end(), myCompareFunction);

	std::string result = "[";

	if (dist0.first != 0)
		result += "{\"word\":\"" + dist0.second + "\",\"freq\":" + std::to_string(dist0.first) + ",\"distance\":0}";


	if (dist0.first != 0 && dist1.size() > 0)
		result += ",";

	if (dist1.size() > 0)
		result += "{\"word\":\"" + dist1[0].second + "\",\"freq\":" + std::to_string(dist1[0].first) + ",\"distance\":1}";

	for (int i = 1; i < dist1.size(); ++i)
	{
		result += ",";
		result += "{\"word\":\"" + dist1[i].second + "\",\"freq\":" + std::to_string(dist1[i].first) + ",\"distance\":1}";
	}

	if ((dist0.first != 0 || dist1.size() > 0) && dist2.size() > 0)
		result += ",";

	if (dist2.size() > 0)
		result += "{\"word\":\"" + dist2[0].second + "\",\"freq\":" + std::to_string(dist2[0].first) + ",\"distance\":2}";


	for (int i = 1; i < dist2.size(); ++i)
	{
		result += ",";
		result += "{\"word\":\"" + dist2[i].second + "\",\"freq\":" + std::to_string(dist2[i].first) + ",\"distance\":2}";
	}

	result += "]";
	std::cout << result << std::endl;

	dist0 = std::make_pair(0, "");
	dist1.clear();
	dist2.clear();
	dist3.clear();

}

FileNode* App::openDict(std::string filename) {
  int fd = open(filename.c_str(), O_RDONLY);
  struct stat sbuf;
  stat(filename.c_str(), &sbuf);
  int pagesize = getpagesize();
  char* data = (char*) mmap((caddr_t)0, sbuf.st_size, PROT_READ, MAP_PRIVATE, fd, 0*pagesize);

  FileNode* fn = (FileNode*) data;
  close(fd);

  return (FileNode*) data;
}

void App::searchWordUNREC(FileNode* root, char* word, int distance) {
  // word of length n
  unsigned int n = strlen(word);

  // dist[COL][ROW], dimension dist[n][n+2]
  // n+2 columns: n letters, plus one from the algorithm (empty word)
  // and a last one to store the word as we go through the tree
  unsigned char** dist = (unsigned char**) malloc((n+2) * sizeof(unsigned char*));
  for (int i = 0; i < n + 2; ++i) {
    dist[i] = (unsigned char*) malloc((n + distance + 2) * sizeof(unsigned char));
  }
  // Initialize the distance table
  for (int i = 0; i <= n; ++i) {
    dist[i][0] = i;
    dist[0][i] = i;
  }
  dist[n+1][0] = n+1;
  for (int i = 1; i <= distance + 1; ++i)
    dist[0][n+i] = n + i;


  // The depth at which we are currently exploring the tree
  unsigned char depth = 0;

  // vectIterator - states of the iterator to iterate on the brothers (vect)
  unsigned char* vectIterators = (unsigned char*) malloc(MAX_TREE_DEPTH * sizeof(unsigned char));
  for (int i = 0; i < MAX_TREE_DEPTH; ++i) {
    vectIterators[i] = 0;
  }

  // nodes - store which node we are exploring at which depth of the tree
  FileNode** nodes = (FileNode**) malloc(MAX_TREE_DEPTH * sizeof(FileNode*));
  for (int i = 0; i < MAX_TREE_DEPTH; ++i) {
    nodes[i] = root;
  }

  while (depth != 0 || vectIterators[depth] < nodes[depth]->directChildrenNum) {
    // "Derecurse"
    while (vectIterators[depth] >= nodes[depth]->directChildrenNum && depth > 0) {
      --depth;
      ++vectIterators[depth];
    }
    // If we got out of the previous while loop because of the second condition (depth > 0),
    // we need to POTENTIALLY ignore all computations (if we were iterating on the last child of the root)
    if (vectIterators[depth] >= nodes[depth]->directChildrenNum) {
      continue;
    }

    if (vectIterators[depth] == 0) {
      // If this is the first iteration, the children is right next to the parent node
      nodes[depth+1] = nodes[depth] + 1;
    }
    else {
      // Else, we already have the previous sibling node and have to use the stored offset to go to the next one
      nodes[depth+1] += nodes[depth+1]->childrenNum;
    }

    // We store the character to form the word as we explore the tree
    dist[n+1][depth] = fromDataFormat[nodes[depth+1]->character];

    // A step of the Damerau-Levenshtein distance algorithm
    computeDistanceUNREC(depth+1, fromDataFormat[nodes[depth+1]->character], fromDataFormat[nodes[depth]->character], word, n, dist);

    // If the minimum distance is lesser than the requested one, there is a chance to find matching words
    if (minimumDistanceUNREC(dist, n+1, depth+1) <= distance) {
      // If the node itself mark the end of a word ...
      if (nodes[depth+1]->data > 0) {
       unsigned char lastDistance = dist[n][depth+1];
       
       // ... and is at a correct distance, add it to our list
       if (lastDistance <= distance) {

        dist[n+1][depth+1] = '\0';
        std::string matchingWord((char*)((void*)dist[n+1]));

	uint32_t foundData = nodes[depth+1]->data;
         if (lastDistance == 0) {
	   
           dist0 = std::make_pair(foundData, matchingWord); }
           else if (lastDistance == 1)
             dist1.push_back(std::make_pair(foundData, matchingWord));
           else if (lastDistance == 2)
             dist2.push_back(std::make_pair(foundData, matchingWord));
           else
             dist3.push_back(std::make_pair(foundData, matchingWord));
         }
       }

      // Then go one level deeper in the tree
      if (depth < n + distance) {
         // "Recurse"
      	 depth++;
      	 vectIterators[depth] = -1;
       }
     }

     ++vectIterators[depth];
   }

  // Freeing the memory is trivial in a non-recursive method
  for (int i = 0; i < n + 2; ++i) {
    free(dist[i]);
  }
  free(dist);
  free(vectIterators);
  free(nodes);
 }

void App::computeDistanceUNREC(int ind, char letter, char prev_letter, char* word, int size_word, unsigned char**dist) {
  unsigned char cost = 0;

  for (int i = 1; i <= size_word; ++i) {
    cost = !(letter == word[i-1]);
    dist[i][ind] = std::min(dist[i][ind-1], dist[i-1][ind]) + 1;
    dist[i][ind] = std::min(dist[i][ind], (unsigned char)(dist[i-1][ind-1] + cost));

    if (i > 1 && ind > 1 && letter == word[i - 2] && prev_letter == word[i - 1]) {
      dist[i][ind] = std::min(dist[i][ind], (unsigned char)(dist[i-2][ind-2] + cost));
    }
  }

}

unsigned char App::minimumDistanceUNREC(unsigned char** dist, unsigned char size, unsigned char ind) {
  unsigned char minimum = dist[0][ind];

  for (int i = 1; i < size; ++i) {
    if (dist[i][ind] < minimum) {
      minimum = dist[i][ind];
    }      
  }

  return minimum;
}
