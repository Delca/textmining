
all: compiler app

compiler:
	clear
	g++ src/compiler/*.cc -o TextMiningCompiler -std=c++0x

app:
	clear
	g++ src/compiler/compiler.cc src/compiler/node.cc src/app/*.cc -o TextMiningApp -std=c++0x -Ofast

check: all
	./TextMiningCompiler words.txt DICT
	cd check && ./main.py

clean:
	rm -f *~
	rm -f TextMiningCompiler TextMiningApp
	cd check && ./clean.sh

doc:
	make -C doc/

.PHONY: compiler app clean doc
