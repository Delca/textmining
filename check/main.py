#!/usr/bin/env python

import opt_parser
import sys
import tests
import os

categories = []
array = []
# getting the command line from arguments
cmd_line = opt_parser.argv_cmd(sys.argv)
i = 0
directory = os.path.dirname(sys.argv[0])


if not cmd_line:
    categories = opt_parser.cat_fill(directory)
    array = [0, 0, 0, 0, 1, 0, [], 1, 0, 0]
else:
    array = opt_parser.opt_error(cmd_line)
    categories = []


    if array == 0:
        print "Command line error.\n\tUsage : ./check/main.py [-e <category+>] [-c|f|n|a|k|z]"
        array = [0, 0, 42, 0, 1, 0, [], 1, 0, 0]
    else:
        if array[1] >= 1:
            i = opt_parser.cat_error(array[6], directory)
            if i == 1:
                print "Wrong category"
            else:
                categories = array[6]
                opt_parser.cat_path(categories, directory)
        else:
            categories = opt_parser.cat_fill(directory)

tests.path = directory

# DISPLAY STARTING
if array[9] == 1:
    tests.apple_bloom_ftw()
if array[8] == 1:
    tests.printpine()
if array[5] == 1:
    tests.print_title2()
if categories:
    tests.print_boxed_style('TextMining Test Suite', 10, array[5])
if categories and array[2] == 1:
    tests.print_separator(80)
    print ""
tot = [0, 0]

for f in categories:
    name = f.split("/")[-1]
    # if the option -f is disabled
    if array[2] == 0:
        tests.print_boxed_style(name[0].upper() + name[1:], 80, array[5])
        tests.print_format()
        tests.print_separator(80)
        print ""
    # array of 2 int
    # do test in 'f' category if not(cat or fin) and disp
    cat_tot = tests.do_all_test(f, not (array[0] or array[2]), array[7])

    tot[0] += cat_tot[0]
    tot[1] += cat_tot[1]

    if array[2] == 0:
        print("\n             "),
        opt_parser.color_right(cat_tot[0], cat_tot[1])
        if array[3] == 0:
            opt_parser.percentit(cat_tot)
            print "Subtotal : " + str(cat_tot[0]) + '% tests passed\033[0;m\n'
        else:
            if cat_tot[0] > 1:
                print "Subtotal : " + str(cat_tot[0]) + '/' + str(cat_tot[1]) + ' tests passed\033[0;m\n'
            else:
                print "Subtotal : " + str(cat_tot[0]) + '/' + str(cat_tot[1]) + ' test passed\033[0;m\n'
        tests.print_separator(80)
        print ""

# DISPLAY TOTAL OF SUCCESS

if array[3] == 0:
    opt_parser.percentit(tot)
if categories:
    print("             "),
    opt_parser.color_right(tot[0], tot[1])
    if array[3] == 0:
        print "Total : " + str(tot[0]) + '% tests passed\033[0;m\n'
    else:
        if tot[0] > 1:
            print "Total : " + str(tot[0]) + '/' + str(tot[1]) + ' tests passed\033[0;m\n'
        else:
            print "Total : " + str(tot[0]) + '/' + str(tot[1]) + ' test passed\033[0;m\n'

if categories:
    tests.print_separator(80)


print "\033[0;m"

