import json
import os
import subprocess
import difflib
import sys
import glob
import shlex
import os.path

name_len = 60
mouli_length = 80
path = ""

def decode (text, enc='utf8'):
    return text.decode('utf8')

#### -COMPARES OUTPUT WITH REFERENCE OUTPUT- ####

def do_test_output(arg1, display=1, display_success=1, cat=0, retval_only=False):
    f = open(arg1)
    fjs = json.load(f)
    f.close()

    options = fjs['bin']

    ref_retval = fjs['retval']
    ref_stdout = fjs['stdout']
    ref_stderr = fjs['stderr']

    if fjs['options'] != None:
        options = [options, fjs['options']]

    my_stdin = ""
    if fjs['stdin'] != None:
        with open(os.path.join(os.path.dirname(arg1), fjs['stdin']), 'r') as stdinFile:
            my_stdin = stdinFile.read()

    if (cat == 1):
        options = ["./cat_test.sh", fjs['bin'], arg1.replace(".js", "")]
        ref_options = ["./cat_test.sh", "/u/all/boisse_r/public/bistromatique", arg1.replace(".js", "")]
        ref_process = subprocess.Popen(ref_options, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        ref_stdout, ref_stderr = ref_process.communicate()
        ref_retval = ref_process.returncode

    if (cat == 2):
        with open(os.path.join(os.path.dirname(arg1), fjs['stdout']), 'r') as stdinFile:
            ref_stdout = stdinFile.read()

    if fjs['stdin'] != None:
        process = subprocess.Popen(options, stdout=subprocess.PIPE, stderr=subprocess.PIPE, stdin=subprocess.PIPE)
        process.stdin.write(my_stdin);
    else:
        process = subprocess.Popen(options, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

    out, err = process.communicate()

    retval = process.returncode

    d_err = 1
    d_out = (out == ref_stdout)

#    d = difflib.Differ()
#    sys.stdout.writelines(list(d.compare(err, fjs['stderr'])))

#    de = difflib.SequenceMatcher(None, err, fjs['stderr'])
#    print de.ratio()

    success = retval == ref_retval and (retval_only or (d_out == 1.0 and d_err == 1.0))

    if display == 0:
        return success


# Starting display

    s = ""

    if not(success):
        s = "\033[0;31m[\033[5;31mFAILURE\033[0;31m]"
    elif display_success == 1:
        s = "\033[1;34m[SUCCESS]"

    filename = arg1.split('/')[-1]
    if len(fjs['name']) + len(filename) > name_len - len(filename):
        fjs['name'] = fjs['name'][:name_len - len(filename) - 13] + "..."

    s = s + "\033[0;90m ("+filename+")%-{0}s\033[0;m".format(max(name_len, len(fjs['name'])) - len(filename) - 10) % fjs['name']

    if retval == ref_retval:
        s = s + "\033[0;32m[O]\033[0;m "
    else:
        s = s + "\033[0;31m[X]\033[0;m "

    if d_out == 1.0 or retval_only:
        s = s + "\033[0;32m[O]\033[0;m "
    else:
        s = s + "\033[0;31m[X]\033[0;m "

    if d_err == 1.0 or retval_only:
        s = s + "\033[0;32m[O]\033[0;m"
    else:
        s = s + "\033[0;31m[X]\033[0;m"


    insert = max(0, (mouli_length - name_len - 15) / 2 + (mouli_length - name_len - 15) % 2)
    print_insert(insert)

    if not(success):
        print s
        if display_success == 0:
            if retval != ref_retval:
                print "Valeurs de retour differentes : ref = " + str(ref_retval) + " / notre : " + str(retval)
            if not d_out:
                print "Sorties standard differentes :\nref = " + ref_stdout + "\nour = " + out
            if not d_err:
                print "Sorties d'erreur differentes :\nref = " + ref_stderr + "\nour = " + err
    elif display_success == 1:
        print s

    return success



#### -END OF do_test_output- ####

#### -COMPARES OUTPUT WITH BASH OUTPUT- ####
1
def do_test_bash(arg1, display=1, display_success=1):
    f = open(arg1)
    fjs = json.load(f)
    f.close()

    options = [path + "/" + fjs['bin'], "-c"]
    bash_options = ["bash", "-c"]

    if fjs['options'] != None:
        options = options + map(decode, shlex.split(fjs['options'].encode('utf8')))
        bash_options = bash_options + map(decode, shlex.split(fjs['options'].encode('utf8')))

    process = subprocess.Popen(options, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, err = process.communicate()
    retval = process.returncode

    bash_process = subprocess.Popen(bash_options, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    bash_out, bash_err = bash_process.communicate()
    bash_retval = bash_process.returncode

    if bash_retval > 0 and retval == bash_retval:
        d_err = True
        d_out = True
    else:
        d_err = (err == bash_err)
        d_out = (out == bash_out)


# Starting display

    if display == 0:
        return not(retval != bash_retval or d_out != True or d_err != True)

    s = ""

    if retval != bash_retval or d_out != True or d_err != True:
        s = "\033[0;31m[\033[5;31mFAILURE\033[0;31m]"
    elif display_success == 1:
        s = "\033[1;34m[SUCCESS]"

    filename = arg1.split('/')[-1]
    if len(fjs['name']) + len(filename) > name_len - len(filename):
        fjs['name'] = fjs['name'][:name_len - len(filename) - 13] + "..."

    s = s + "\033[0;90m ("+filename+")%-{0}s\033[0;m".format(max(name_len, len(fjs['name'])) - len(filename) - 10) % fjs['name']

    if retval == bash_retval:
        s = s + "\033[0;32m[O]\033[0;m "
    else:
        s = s + "\033[0;31m[X]\033[0;m "

    if d_out == 1.0:
        s = s + "\033[0;32m[O]\033[0;m "
    else:
        s = s + "\033[0;31m[X]\033[0;m "

    if d_err == 1.0:
        s = s + "\033[0;32m[O]\033[0;m"
    else:
        s = s + "\033[0;31m[X]\033[0;m"

    insert = max(0, (mouli_length - name_len - 15) / 2 + (mouli_length - name_len - 15) % 2)
    print_insert(insert)

    if retval != bash_retval or d_out != True or d_err != True:
        print s
        if display_success == 0:
            if retval != bash_retval:
                print "Valeurs de retour differentes : ref = " + str(bash_retval) + " / notre : " + str(retval)
            if not d_out:
                print "Sorties standard differentes :\nref = " + bash_out + "\nour = " + out
            if not d_err:
                print "Sorties d'erreur differentes :\nref = " + bash_err + "\nour = " + err
    elif display_success == 1:
        print s


    return not(retval != bash_retval or d_out != True or d_err != True)

#### -END OF do_test_bash- ####

#### -DETERMINES AND PERFORM THE RIGHT TEST TYPE - ####

def do_test(filename, display=1, disp_succ=1):
    f = open(filename)
    fjs = json.load(f)
    f.close()

    if (fjs['type'] == "compare_output"):
        return do_test_output(filename, display, disp_succ, 0)
    if (fjs['type'] == "cat_compare_output"):
        return do_test_output(filename, display, disp_succ, 1)
    if (fjs['type'] == "compare_retval"):
        return do_test_output(filename, display, disp_succ, 0, True)
    if (fjs['type'] == "compare_bash"):
        return do_test_bash(filename, display, disp_succ)
    if (fjs['type'] == "compare_ref_output"):
        return do_test_output(filename, display, disp_succ, 2)

#### -END OF do_test- ####

#### -DO ALL THE TESTS !- ####

def do_all_test(category='default', display=1, disp_succ=1):
    tests = glob.glob(category + '/*.js')

    tests.sort()

    success = 0

    for i in tests:
        success = success + do_test(i, display, disp_succ)

    return [success, len(tests)]

#### -END OF do_all_test- ####

#### -PRINTS A BOXED TEXT- ####

def my_little_printf(text='Some text', dec = 0, kikoo=0):
    for i in range(len(text)):
        color = '\033[0;{0};{1}m'.format(((i + dec) % 7 + 41), (i + dec + 5) % 7 + 31)
        if kikoo != 0:
            sys.stdout.write(color);
        sys.stdout.write(text[i]),
        if kikoo != 0:
            sys.stdout.write('\033[0;29m')
        else:
            sys.stdout.write('\033[0;29m')

    return dec + len(text)

def print_line(char='#', length=80, borderchar='#'):
    sys.stdout.write(borderchar),
    for i in range(length - 2):
        sys.stdout.write(char),
    sys.stdout.write(borderchar),
    sys.stdout.write("\n"),

def print_blankline(char='#', length=80, borderchar='#', kikoo=0):
    sys.stdout.write(borderchar),
    dec = 0
    for i in range(length - 2):
        color = '\033[0;{0};{1}m'.format(((i + dec) % 7 + 41), (i + dec + 5) % 7 + 31)
        if kikoo != 0:
            sys.stdout.write(color);
        sys.stdout.write(" "),
        sys.stdout.write('\033[0;m')
    sys.stdout.write(borderchar),
    sys.stdout.write("\n"),

def print_textline(char='\033[0;m#\033[0;m', text='SOME TEXT', length=80, borderchar='\033[0;m#\033[0;m', kikoo=0):
    spaces = (length - 2 - len(text)) / 2
    dec = 0
    sys.stdout.write(borderchar),
    for i in range(spaces):
        color = '\033[0;{0};{1}m'.format(((i) % 7 + 41), (i + 5) % 7 + 31)
        dec += 1
        if kikoo != 0:
            sys.stdout.write(color)
        sys.stdout.write(" "),
    sys.stdout.write("\033[0;29m"),
#    print(text),
    dec = my_little_printf(text, dec, kikoo)
    sys.stdout.write("\033[0;m"),
    for i in range(spaces + (length - 2 - len(text)) % 2):
        color = '\033[0;{0};{1}m'.format(((i + dec) % 7 + 41), (i + dec + 5) % 7 + 31)
        if kikoo != 0:
            sys.stdout.write(color)
        sys.stdout.write(" ")
    sys.stdout.write(borderchar),
    sys.stdout.write("\n"),

def print_insert(length=20):
    for i in range(length):
        sys.stdout.write(" "),

def print_boxed(text='SOME TEXT', minlength=80, char='#', cornerchar='NOPE', borderchar='NOPE', kikoo=0):
    length = max(minlength, len(text) + 8)
    insert = max(0, (80 - length) / 2 + (80 - length) % 2)
    if cornerchar == 'NOPE':
        cornerchar = char
    if borderchar == 'NOPE':
        borderchar = char
    print_insert(insert)
    print_line(char, length, cornerchar)
    print_insert(insert)
    print_blankline(char, length, borderchar, kikoo)
    print_insert(insert)
    print_textline(char, text, length, borderchar, kikoo);
    print_insert(insert)
    print_blankline(char, length, borderchar, kikoo)
    print_insert(insert)
    print_line(char, length, cornerchar)
    print("\n"),

def print_boxed_style(text='SOME TEXT', minlength=80, kikoo=0):
    print_boxed(text, minlength, '\033[0;31;44m-\033[0;m', '\033[0;31;44m+\033[0;m', '\033[0;31;44m|\033[0;m', kikoo)

def print_format(length=mouli_length):
    #    insert = max(0, (80 - length) / 2 + (80 - length) % 2)
    insert = max(0, (mouli_length - name_len - 15) / 2 + (mouli_length - name_len - 16) % 2 + 1)
    print_insert(insert)
    s = "\033[0;29m[TSTATUS] %{0}s [R] [O] [E]\033[0;m".format(name_len - 9)
    s = s % " "
    print s

def print_separator(length=80, char="\033[0;35m-\033[0;m", borderchar="\033[0;33m+\033[0;m"):
    sys.stdout.write(borderchar)
    for i in range(length - 2):
        sys.stdout.write(char)
    sys.stdout.write(borderchar)
    sys.stdout.write("\n")


# Added the display of title in case of kikoo
def print_title():

    print ""
    my_little_printf("                             ")
    my_little_printf("/", 0, 1)
    my_little_printf("     ")
    my_little_printf("/\"\"\"\"\\", 0, 1)

    print ""

    my_little_printf("                            ")
    my_little_printf("/", 0, 1)
    my_little_printf("      ")
    my_little_printf("\\", 0, 1)
    my_little_printf("    ")
    my_little_printf("/", 0, 1)
    my_little_printf("   ")
    my_little_printf("####", 0, 1)
    my_little_printf("  ")
    my_little_printf("#", 0, 1)
    my_little_printf("   ")
    my_little_printf("#", 0, 1)

    print ""

    my_little_printf("                           ")
    my_little_printf("/", 0, 1)
    my_little_printf("           ")
    my_little_printf("/", 0, 1)
    my_little_printf("   ")
    my_little_printf("#", 0, 1)
    my_little_printf("      ")
    my_little_printf("#", 0, 1)
    my_little_printf("   ")
    my_little_printf("#", 0, 1)

    print ""

    my_little_printf("                          ")
    my_little_printf("L___|__", 0, 1)
    my_little_printf("     ")
    my_little_printf("/", 0, 1)
    my_little_printf("     ")
    my_little_printf("####", 0, 1)
    my_little_printf("  ")
    my_little_printf("#####", 0, 1)

    print ""

    my_little_printf("                              ")
    my_little_printf("T", 0, 1)
    my_little_printf("      ")
    my_little_printf("/", 0, 1)
    my_little_printf("          ")
    my_little_printf("#", 0, 1)
    my_little_printf(" ")
    my_little_printf("#", 0, 1)
    my_little_printf("   ")
    my_little_printf("#", 0, 1)

    print ""

    my_little_printf("                              ")
    my_little_printf("|", 0, 1)
    my_little_printf("     ")
    my_little_printf("/_____", 0, 1)
    my_little_printf("  ")
    my_little_printf("####", 0, 1)
    my_little_printf("  ")
    my_little_printf("#", 0, 1)
    my_little_printf("   ")
    my_little_printf("#", 0, 1)

    print ""
    print ""


def print_title2():
    return

def old_print_title2():
    print ""
    my_little_printf("                             ")
    my_little_printf("/", 29, 1)
    my_little_printf("     ")
    my_little_printf("/\"\"\"\"\\", 4, 1)

    print ""

    my_little_printf("                            ")
    my_little_printf("/", 28, 1)
    my_little_printf("      ")
    my_little_printf("\\", 6, 1)
    my_little_printf("    ")
    my_little_printf("/", 4, 1)
    my_little_printf("   ")
    my_little_printf("####", 7, 1)
    my_little_printf("  ")
    my_little_printf("#", 9, 1)
    my_little_printf("   ")
    my_little_printf("#", 5, 1)

    print ""

    my_little_printf("                           ")
    my_little_printf("/", 26, 1)
    my_little_printf("           ")
    my_little_printf("/", 14, 1)
    my_little_printf("   ")
    my_little_printf("#", 3, 1)
    my_little_printf("      ")
    my_little_printf("#", 8, 1)
    my_little_printf("   ")
    my_little_printf("#", 10, 1)

    print ""

    my_little_printf("                          ")
    my_little_printf("L___|__", 28, 1)
    my_little_printf("     ")
    my_little_printf("/", 5, 1)
    my_little_printf("     ")
    my_little_printf("####", 9, 1)
    my_little_printf("  ")
    my_little_printf("#####", 7, 1)

    print ""

    my_little_printf("                              ")
    my_little_printf("T", 31, 1)
    my_little_printf("      ")
    my_little_printf("/", 1, 1)
    my_little_printf("          ")
    my_little_printf("#", 10, 1)
    my_little_printf(" ")
    my_little_printf("#", 10, 1)
    my_little_printf("   ")
    my_little_printf("#", 7, 1)

    print ""

    my_little_printf("                              ")
    my_little_printf("|", 5, 1)
    my_little_printf("     ")
    my_little_printf("/_____", 2, 1)
    my_little_printf("  ")
    my_little_printf("####", 3, 1)
    my_little_printf("  ")
    my_little_printf("#", 11, 1)
    my_little_printf("   ")
    my_little_printf("#", 8, 1)

    print ""
    print ""


def printpine():
    print ""
    print_insert(22)
    print("                "),
    print("\033[2;32m*\033[1;m")
    print_insert(22)
    print("                "),
    for i in range(3):
        sys.stdout.write("\033[2;32m*\033[1;m")

    print ""

    print_insert(22)
    print("               "),
    for i in range(3):
        sys.stdout.write("\033[2;32m*\033[1;m")
    sys.stdout.write("\033[1;33mO\033[1;m")
    sys.stdout.write("\033[2;32m*\033[1;m")
    print ""

    print_insert(22)
    print("              "),
    for i in range(2):
        sys.stdout.write("\033[2;32m*\033[1;m")
    sys.stdout.write("\033[5;34mO\033[1;m")
    for i in range(4):
        sys.stdout.write("\033[2;32m*\033[1;m")

    print ""

    print_insert(22)
    print("               "),
    for i in range(5):
        sys.stdout.write("\033[2;32m*\033[1;m")

    print ""

    print_insert(22)
    print("              "),
    sys.stdout.write("\033[2;32m*\033[1;m")
    sys.stdout.write("\033[5;31mO\033[1;m")
    for i in range(5):
        sys.stdout.write("\033[2;32m*\033[1;m")

    print ""

    print_insert(22)
    print("             "),
    for i in range(7):
        sys.stdout.write("\033[2;32m*\033[1;m")

    sys.stdout.write("\033[1;35mO\033[1;m")
    sys.stdout.write("\033[2;32m*\033[1;m")

    print ""


    print_insert(22)
    print("            "),
    for i in range(11):
        sys.stdout.write("\033[2;32m*\033[1;m")

    print ""

    print_insert(22)
    print("             "),
    for i in range(9):
        sys.stdout.write("\033[2;32m*\033[1;m")

    print ""

    print_insert(22)
    print("            "),
    for i in range(2):
        sys.stdout.write("\033[2;32m*\033[1;m")
    sys.stdout.write("\033[5;36mO\033[1;m")
    for i in range(8):
        sys.stdout.write("\033[2;32m*\033[1;m")

    print ""

    print_insert(22)
    print("           "),
    for i in range(13):
        sys.stdout.write("\033[2;32m*\033[1;m")

    print ""

    print_insert(22)
    print("          "),
    for i in range(12):
        sys.stdout.write("\033[2;32m*\033[1;m")
    sys.stdout.write("\033[1;33mO\033[1;m")
    for i in range(2):
        sys.stdout.write("\033[2;32m*\033[1;m")

    print ""

    print_insert(22)
    print("         "),
    for i in range(8):
        sys.stdout.write("\033[2;32m*\033[1;m")
    sys.stdout.write("\033[1;34mO\033[1;m")
    for i in range(8):
        sys.stdout.write("\033[2;32m*\033[1;m")

    print ""

    print_insert(22)
    print("          "),
    for i in range(15):
        sys.stdout.write("\033[2;32m*\033[1;m")

    print ""

    print_insert(22)
    print("        "),
    for i in range(3):
        sys.stdout.write("\033[2;32m*\033[1;m")
    sys.stdout.write("\033[1;35mO\033[1;m")
    for i in range(15):
        sys.stdout.write("\033[2;32m*\033[1;m")

    print ""

    print_insert(22)
    print("      "),
    for i in range(19):
        sys.stdout.write("\033[2;32m*\033[1;m")
    sys.stdout.write("\033[5;31mO\033[1;m")
    for i in range(3):
        sys.stdout.write("\033[2;32m*\033[1;m")
    print ""

    print_insert(22)
    print("    "),
    for i in range(27):
        sys.stdout.write("\033[2;32m*\033[1;m")

    print ""

    print_insert(22)
    print("               "),
    for i in range(5):
        sys.stdout.write("\033[7;31m|\033[1;m")

    print ""
    print_insert(22)
    print("               "),
    for i in range(5):
        sys.stdout.write("\033[7;31m|\033[1;m")

    print ""
    print_insert(22)
    print("               "),
    for i in range(5):
        sys.stdout.write("\033[7;31m|\033[1;m")
    print ""
    print_insert(22)
    print("               "),
    for i in range(5):
        sys.stdout.write("\033[7;31m|\033[1;m")

    print ""
    print_insert(22)
    print("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^")
    print ""

def apple_bloom_ftw():
    indent = 8

    print_insert(indent)
    print "\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[2;35mo\033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m"
    print_insert(indent)
    print "\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[2;35mo\033[0m\033[1;35m.\033[0m\033[2;35mo\033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m"
    print_insert(indent)
    print "\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[2;35mo\033[0m\033[2;35mo\033[0m\033[2;35mo\033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[2;35mo\033[0m\033[1;35m.\033[0m\033[1;35m.\033[0m\033[2;35mo\033[0m\033[2;35mo\033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m"
    print_insert(indent)
    print "\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[2;35mo\033[0m\033[2;35mo\033[0m\033[1;35m.\033[0m\033[1;35m.\033[0m\033[2;35mo\033[0m\033[2;35mo\033[0m\033[2;35mo\033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[2;35mo\033[0m\033[1;35m.\033[0m\033[1;35m.\033[0m\033[1;35m.\033[0m\033[2;35mo\033[0m\033[1;35m.\033[0m\033[2;35mo\033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m"
    print_insert(indent)
    print "\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[2;35mo\033[0m\033[1;35m.\033[0m\033[2;35mo\033[0m\033[1;35m.\033[0m\033[1;35m.\033[0m\033[1;35m.\033[0m\033[1;35m.\033[0m\033[2;35mo\033[0m\033[2;35mo\033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[2;35mo\033[0m\033[1;35m.\033[0m\033[1;35m.\033[0m\033[1;35m.\033[0m\033[2;35mo\033[0m\033[1;35m.\033[0m\033[2;35mo\033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m"
    print_insert(indent)
    print "\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[2;35mo\033[0m\033[1;35m.\033[0m\033[1;35m.\033[0m\033[2;35mo\033[0m\033[1;35m.\033[0m\033[1;35m.\033[0m\033[1;35m.\033[0m\033[1;35m.\033[0m\033[1;35m.\033[0m\033[2;35mo\033[0m\033[2;35mo\033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[2;35mo\033[0m\033[1;35m.\033[0m\033[1;35m.\033[0m\033[2;35mo\033[0m\033[1;35m.\033[0m\033[1;35m.\033[0m\033[1;35m.\033[0m\033[2;35mo\033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m"
    print_insert(indent)
    print "\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[2;35mo\033[0m\033[1;35m.\033[0m\033[1;35m.\033[0m\033[2;35mo\033[0m\033[1;35m.\033[0m\033[1;35m.\033[0m\033[1;35m.\033[0m\033[1;35m.\033[0m\033[1;35m.\033[0m\033[1;35m.\033[0m\033[2;35mo\033[0m\033[2;35mo\033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[2;35mo\033[0m\033[1;35m.\033[0m\033[1;35m.\033[0m\033[1;33m\033[0;33m@\033[0m\033[0m\033[1;35m.\033[0m\033[1;35m.\033[0m\033[1;35m.\033[0m\033[1;35m.\033[0m\033[2;35mo\033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m"
    print_insert(indent)
    print "\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[2;35mo\033[0m\033[1;35m.\033[0m\033[1;35m.\033[0m\033[1;35m.\033[0m\033[2;35mo\033[0m\033[2;35mo\033[0m\033[1;35m.\033[0m\033[1;35m.\033[0m\033[1;35m.\033[0m\033[1;35m.\033[0m\033[1;35m.\033[0m\033[1;35m.\033[0m\033[1;35m.\033[0m\033[2;35mo\033[0m\033[0m \033[0m\033[2;35mo\033[0m\033[1;35m.\033[0m\033[1;33m\033[0;33m@\033[0m\033[0m\033[1;33m#\033[0m\033[1;33m\033[0;33m@\033[0m\033[0m\033[1;35m.\033[0m\033[1;35m.\033[0m\033[1;35m.\033[0m\033[2;35mo\033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[2;31m/\033[0m\033[2;31m/\033[0m\033[2;31m/\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m"
    print_insert(indent)
    print "\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[2;35mo\033[0m\033[1;35m.\033[0m\033[1;35m.\033[0m\033[1;35m.\033[0m\033[1;35m.\033[0m\033[2;35mo\033[0m\033[2;35mo\033[0m\033[1;35m.\033[0m\033[1;35m.\033[0m\033[1;35m.\033[0m\033[1;35m.\033[0m\033[2;35mo\033[0m\033[2;35mo\033[0m\033[2;35mo\033[0m\033[1;35m.\033[0m\033[1;33m\033[0;33m@\033[0m\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m\033[0;33m@\033[0m\033[0m\033[1;35m.\033[0m\033[2;35mo\033[0m\033[0m \033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[2;31m/\033[0m\033[2;31m/\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[2;31m/\033[0m\033[2;31m/\033[0m\033[2;31m/\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m"
    print_insert(indent)
    print "\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[2;35mo\033[0m\033[1;35m.\033[0m\033[1;35m.\033[0m\033[1;35m.\033[0m\033[1;35m.\033[0m\033[1;35m.\033[0m\033[1;35m.\033[0m\033[2;35mo\033[0m\033[2;35mo\033[0m\033[2;35mo\033[0m\033[2;35mo\033[0m\033[1;35m.\033[0m\033[1;35m.\033[0m\033[1;35m.\033[0m\033[1;33m\033[0;33m@\033[0m\033[0m\033[1;33m#\033[0m\033[0;33m@\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m\033[0;33m@\033[0m\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[2;31m/\033[0m\033[2;31m/\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[2;31m/\033[0m\033[2;31m/\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m"
    print_insert(indent)
    print "\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[2;35mo\033[0m\033[1;35m.\033[0m\033[1;35m.\033[0m\033[1;35m.\033[0m\033[1;35m.\033[0m\033[1;35m.\033[0m\033[1;35m.\033[0m\033[1;35m.\033[0m\033[1;35m.\033[0m\033[1;35m.\033[0m\033[1;35m.\033[0m\033[1;35m.\033[0m\033[1;35m.\033[0m\033[1;33m\033[0;33m@\033[0m\033[0m\033[1;33m#\033[0m\033[0;33m@\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m\033[0;33m@\033[0m\033[0m\033[1;33m#\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[2;31m/\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m"
    print_insert(indent)
    print "\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[2;35mo\033[0m\033[2;35mo\033[0m\033[1;35m.\033[0m\033[1;35m.\033[0m\033[1;35m.\033[0m\033[1;35m.\033[0m\033[1;35m.\033[0m\033[1;35m.\033[0m\033[1;35m.\033[0m\033[1;35m.\033[0m\033[1;35m.\033[0m\033[2;35mo\033[0m\033[1;33m\033[0;33m@\033[0m\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[0;33m@\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;33m#\033[0m\033[1;31m*\033[0m\033[2;31m/\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[0m \033[0m\033[1;31m*\033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m"
    print_insert(indent)
    print "\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[2;35mo\033[0m\033[2;35mo\033[0m\033[2;35mo\033[0m\033[1;35m.\033[0m\033[1;35m.\033[0m\033[1;35m.\033[0m\033[1;35m.\033[0m\033[2;35mo\033[0m\033[2;35mo\033[0m\033[0m \033[0m\033[0m \033[0m\033[1;33m\033[0;33m@\033[0m\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[0;33m@\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;31m*\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[2;31m/\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m"
    print_insert(indent)
    print "\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[2;35mo\033[0m\033[2;35mo\033[0m\033[2;35mo\033[0m\033[2;35mo\033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[1;33m\033[0;33m@\033[0m\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m"
    print_insert(indent)
    print "\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[1;33m\033[0;33m@\033[0m\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[0m \033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[2;33m@\033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m"
    print_insert(indent)
    print "\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[1;31m*\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[0m \033[0m\033[1;37mO\033[0m\033[1;32mO\033[0m\033[0m \033[0m\033[0m \033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[0m \033[0m\033[1;37mO\033[0m\033[1;32mO\033[0m\033[0m \033[0m\033[2;33m@\033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m"
    print_insert(indent)
    print "\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[0m \033[0m\033[1;37mO\033[0m\033[1;32mO\033[0m\033[0m \033[0m\033[1;37mO\033[0m\033[1;37mO\033[0m\033[0m \033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[0m \033[0m\033[1;37mO\033[0m\033[1;32mO\033[0m\033[0m \033[0m\033[2;33m@\033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m"
    print_insert(indent)
    print "\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[0m \033[0m\033[1;37mO\033[0m\033[1;37mO\033[0m\033[1;32mO\033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[0m \033[0m\033[0m \033[0m\033[2;33m@\033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m"
    print_insert(indent)
    print "\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[2;31mT\033[0m\033[2;31mT\033[0m\033[2;31mT\033[0m\033[2;31mT\033[0m\033[2;31mT\033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[0m \033[0m\033[1;37mO\033[0m\033[1;37mO\033[0m\033[1;32mO\033[0m\033[0m \033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[2;33m@\033[0m\033[2;33m@\033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m"
    print_insert(indent)
    print "\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[2;31mT\033[0m\033[2;31mT\033[0m\033[2;31mT\033[0m\033[2;31mT\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[2;31mT\033[0m\033[2;31mT\033[0m\033[2;31mT\033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[1;31m*\033[0m\033[2;31m/\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[2;33m@\033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m"
    print_insert(indent)
    print "\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[2;31mT\033[0m\033[2;31mT\033[0m\033[2;31mt\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[2;31mT\033[0m\033[2;31mT\033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[2;31m/\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;37m+\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[2;33m@\033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m"
    print_insert(indent)
    print "\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[2;31mT\033[0m\033[2;31mT\033[0m\033[2;31mt\033[0m\033[2;31mt\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[2;31m/\033[0m\033[2;31m/\033[0m\033[2;31m/\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[2;31mT\033[0m\033[2;31mT\033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[2;31m/\033[0m\033[2;31m/\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[2;33m@\033[0m\033[2;33m@\033[0m\033[2;33m@\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;37m+\033[0m\033[1;37m+\033[0m\033[1;37m+\033[0m\033[1;37m+\033[0m\033[2;33m@\033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m"
    print_insert(indent)
    print "\033[0m \033[0m\033[0m \033[0m\033[2;31mT\033[0m\033[2;31mt\033[0m\033[2;31mt\033[0m\033[2;31mT\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[2;31m/\033[0m\033[2;31m/\033[0m\033[2;31m/\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[2;31m/\033[0m\033[2;31m/\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[2;31mT\033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[1;31m*\033[0m\033[2;31m/\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[0;33m@\033[0m\033[2;33m@\033[0m\033[2;33m@\033[0m\033[2;33m@\033[0m\033[1;37m+\033[0m\033[1;37m+\033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m"
    print_insert(indent)
    print "\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[2;31mT\033[0m\033[2;31mT\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[2;31m/\033[0m\033[2;31m/\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[2;31m/\033[0m\033[2;31m/\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[2;31m/\033[0m\033[2;31m/\033[0m\033[1;31m*\033[0m\033[2;31mT\033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[2;31m/\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[0;33m@\033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m"
    print_insert(indent)
    print "\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[2;31mT\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[2;31m/\033[0m\033[2;31m/\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[2;31m/\033[0m\033[2;31m/\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[2;31mT\033[0m\033[2;31mT\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[2;31m/\033[0m\033[1;31m*\033[0m\033[2;31mT\033[0m\033[0m \033[0m\033[0m \033[0m\033[0;33m@\033[0m\033[0;33m@\033[0m\033[0;33m@\033[0m\033[0;33m@\033[0m\033[0;33m@\033[0m\033[0;33m@\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[2;31m/\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[0;33m@\033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m"
    print_insert(indent)
    print "\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[2;31mT\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[2;31m/\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[2;31m/\033[0m\033[2;31m/\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[2;31mT\033[0m\033[2;31mT\033[0m\033[0m \033[0m\033[0m \033[0m\033[2;31mT\033[0m\033[2;31mT\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[0;33m@\033[0m\033[0;33m@\033[0m\033[0;33m@\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[2;31m/\033[0m\033[2;31m/\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[0;33m@\033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m"
    print_insert(indent)
    print "\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[2;31mT\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[2;31m/\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[2;31m/\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[2;31mT\033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[2;31mT\033[0m\033[0;33m@\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[2;31m/\033[0m\033[2;31m/\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[0;33m@\033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m"
    print_insert(indent)
    print "\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[2;31mT\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[2;31m/\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[2;31m/\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[2;31mT\033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0;33m@\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[0;33m@\033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m"
    print_insert(indent)
    print "\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[2;31mT\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[2;31m/\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[2;31m/\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[2;31mT\033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0;33m@\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[0;33m@\033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m"
    print_insert(indent)
    print "\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[2;31mT\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[2;31m/\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[2;31m/\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[2;31mT\033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0;33m@\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[0;33m@\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[0;33m@\033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m"
    print_insert(indent)
    print "\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[2;31mT\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[2;31m/\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[2;31m/\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[2;31mT\033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0;33m@\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[0;33m@\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[0;33m@\033[0m\033[0;33m@\033[0m\033[0;33m@\033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m"
    print_insert(indent)
    print "\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[2;31mT\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[2;31m/\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[2;31mT\033[0m\033[2;31mt\033[0m\033[1;31m*\033[0m\033[2;31mT\033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0;33m@\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[0;33m@\033[0m\033[0;33m@\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[0;33m@\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[0;33m@\033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m"
    print_insert(indent)
    print "\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[2;31mT\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[2;31m/\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[2;31mT\033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[1;33m\033[0;33m@\033[0m\033[0m\033[1;33m\033[0;33m@\033[0m\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m\033[0;33m@\033[0m\033[0m\033[1;33m#\033[0m\033[0;33m@\033[0m\033[0;33m@\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m\033[0;33m@\033[0m\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m\033[0;33m@\033[0m\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[0;33m@\033[0m\033[0;33m@\033[0m\033[0;33m@\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[0;33m@\033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m"
    print_insert(indent)
    print "\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[2;31mT\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[2;31m/\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[2;31mt\033[0m\033[2;31mT\033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[1;33m\033[0;33m@\033[0m\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m\033[0;33m@\033[0m\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[0;33m@\033[0m\033[0;33m@\033[0m\033[0;33m@\033[0m\033[0;33m@\033[0m\033[1;33m\033[0;33m@\033[0m\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m\033[0;33m@\033[0m\033[0m\033[0m \033[0m\033[0;33m@\033[0m\033[0;33m@\033[0m\033[0;33m@\033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0;33m@\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[0;33m@\033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m"
    print_insert(indent)
    print "\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[2;31mT\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[2;31m/\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[2;31mT\033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[1;33m\033[0;33m@\033[0m\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m\033[0;33m@\033[0m\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[0;33m@\033[0m\033[0m \033[0m\033[0m \033[0m\033[1;33m\033[0;33m@\033[0m\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m\033[0;33m@\033[0m\033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0;33m@\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[0;33m@\033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m"
    print_insert(indent)
    print "\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[2;31mT\033[0m\033[2;31mT\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[2;31m/\033[0m\033[1;31m*\033[0m\033[1;31m*\033[0m\033[2;31mT\033[0m\033[2;31mT\033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[1;33m\033[0;33m@\033[0m\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m\033[0;33m@\033[0m\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[0;33m@\033[0m\033[0m \033[0m\033[0m \033[0m\033[1;33m\033[0;33m@\033[0m\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m\033[0;33m@\033[0m\033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0;33m@\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[0;33m@\033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m"
    print_insert(indent)
    print "\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[2;31mT\033[0m\033[2;31mT\033[0m\033[2;31mT\033[0m\033[2;31mT\033[0m\033[2;31mT\033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[1;33m\033[0;33m@\033[0m\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m\033[0;33m@\033[0m\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[0;33m@\033[0m\033[0m \033[0m\033[1;33m\033[0;33m@\033[0m\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m\033[0;33m@\033[0m\033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0;33m@\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[0;33m@\033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m"
    print_insert(indent)
    print "\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[1;33m\033[0;33m@\033[0m\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m\033[0;33m@\033[0m\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[0;33m@\033[0m\033[0m \033[0m\033[1;33m\033[0;33m@\033[0m\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m\033[0;33m@\033[0m\033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0;33m@\033[0m\033[0;33m@\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[0;33m@\033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m"
    print_insert(indent)
    print "\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[1;33m\033[0;33m@\033[0m\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m\033[0;33m@\033[0m\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[0;33m@\033[0m\033[1;33m\033[0;33m@\033[0m\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m\033[0;33m@\033[0m\033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0;33m@\033[0m\033[0;33m@\033[0m\033[0;33m@\033[0m\033[0;33m@\033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m"
    print_insert(indent)
    print "\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[1;33m\033[0;33m@\033[0m\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m\033[0;33m@\033[0m\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[0;33m@\033[0m\033[1;33m\033[0;33m@\033[0m\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m\033[0;33m@\033[0m\033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m"
    print_insert(indent)
    print "\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[1;33m\033[0;33m@\033[0m\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m\033[0;33m@\033[0m\033[0m\033[0;33m@\033[0m\033[0;33m@\033[0m\033[0;33m@\033[0m\033[0;33m@\033[0m\033[0m \033[0m\033[1;33m\033[0;33m@\033[0m\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m#\033[0m\033[1;33m\033[0;33m@\033[0m\033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m"
    print_insert(indent)
    print "\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[1;33m\033[0;33m@\033[0m\033[0m\033[1;33m\033[0;33m@\033[0m\033[0m\033[1;33m\033[0;33m@\033[0m\033[0m\033[1;33m\033[0;33m@\033[0m\033[0m\033[1;33m\033[0;33m@\033[0m\033[0m\033[1;33m\033[0;33m@\033[0m\033[0m\033[1;33m\033[0;33m@\033[0m\033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[1;33m\033[0;33m@\033[0m\033[0m\033[1;33m\033[0;33m@\033[0m\033[0m\033[1;33m\033[0;33m@\033[0m\033[0m\033[1;33m\033[0;33m@\033[0m\033[0m\033[1;33m\033[0;33m@\033[0m\033[0m\033[1;33m\033[0;33m@\033[0m\033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m\033[0m \033[0m"

