#! /bin/bash

category=`basename "$PWD"`

while [ ! $# -eq 0 ]; do

test=`basename $1 .js`
cat $test | ../../../reference/linux64/TextMiningApp ../../../reference/linux64/DICT > $test.ref

rm -rf $test.js

echo -en "{
\"name\": \"$test\",
\"type\": \"compare_ref_output\",

\"bin\": \"../TextMiningApp\",
\"options\": \"../DICT\",
\"stdin\": \"$test\",

\"stdout\": \"$test.ref\",
\"stderr\": \"\",
\"retval\": 0
}
" > $test.js


shift;

done;
