import posixpath
import glob
import sys

def argv_cmd(argv):
    return argv[1:]


# array param : array[cat = 0,    :0
#                     sel = 0,    :1
#                     fin = 0,    :2
#                     num = 0,    :3
#                     all = 0,    :4
#                     kikoo = 0,  :5
#                     caty[],     :6
#                     disp = 1]   :7
# cmd_line : command line
def opt_error(cmd_line):
    i = 0
    array = [0, 0, 0, 0, 0, 0, [], 1, 0, 0]
    for cmd in cmd_line:
        if cmd == "-c" or cmd == "-categories" or cmd == "--categories":
            array[0] += 1
        elif cmd == "-e" or cmd == "-select" or cmd == "--select":
            array[1] += 1
        elif cmd == "-f" or cmd == "-final" or cmd == "--final":
            array[2] += 1
        elif cmd == "-n" or cmd == "-number" or cmd == "--number":
            array[3] += 1
        elif cmd == "-a" or cmd == "-all" or cmd == "--all":
            array[4] += 1
        elif cmd == "-k" or cmd == "-kikoo" or cmd == "--kikoo":
            array[5] += 1
        elif cmd == "-z" or cmd == "-nowin" or cmd == "--nowin":
            array[7] = 0
        elif cmd == "-w" or cmd == "-nowel" or cmd == "--nowel":
            array[8] = 1
        elif cmd == "-p" or cmd == "-pony" or cmd == "--pony":
            array[9] = 1
        elif cmd[0] != "-" and array[1] > 0 and (array[1] == (i)):
            array[6].append(cmd)
            array[1] += 1
        else:
            return 0
        i += 1
    return array

def color_right(ok, maxy):
    if ok > 0 and ((ok + 0.) / (maxy + 0.)) >= 0.9:
        sys.stdout.write("\033[0;32m")
    elif ok > 0 and ((ok + 0.) / (maxy + 0.)) >= 0.5:
        sys.stdout.write("\033[0;33m")
    else:
        sys.stdout.write("\033[0;31m")

def cat_error(array, path):
    for cat in array:
        if not posixpath.exists(path + "/db/" + cat):
            print path + "/db/" + cat + " does not exist"
            return 1
    return 0

def cat_fill(path):
    all_dir = glob.glob(path + '/db/*')
    all_dir.sort()
    return all_dir

def cat_path(cat, path):
    i = 0
    while i < len(cat):
        cat[i] = path + '/db/' + cat[i]
        i += 1

def percentit(tab):
    if tab[1] > 0:
        tab[0] = int((tab[0] + 0.) / tab[1] * 100)
    else:
        tab[0] = 0
    tab[1] = 100
