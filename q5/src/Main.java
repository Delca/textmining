import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import nl.flotsam.xeger.Xeger;


public class Main {

	public static Map<Integer, Integer> resultsByDistance = new HashMap<Integer, Integer>();
	public static Map<Integer, Integer> resultsMeanSize = new HashMap<Integer, Integer>();

	public static void main(String[] args) throws Exception {
		String filePath = "data/toTestRegexFINAL";
		BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(filePath), "UTF-8"));
		String line;
		DamerauLevenshtein dl = new DamerauLevenshtein(1, 1, 1, 1);

		int i = 1;
		while ( (line = br.readLine()) != null) {
			String[] splitted = line.split("\t");
			Xeger generator = new Xeger(splitted[0]);
			++i;//System.out.println(i++ + " " + splitted[0]);

			Set<String> set = new HashSet<String>();
			for (int j = 0; j < 20; ++j) {
				set.add(generator.generate());
			}

			Pattern p = Pattern.compile(splitted[0]);
			for(String s : set) {
				Matcher m = p.matcher(s);

				String correctResult = splitted[1];
				while (m.find()) {
					for (int j = 1; j <= 3; ++j) {
						if (correctResult.contains("$" + j)) {
							String replacement;
							if (j <= m.groupCount()) {
								replacement = (m.group(j)==null?"":m.group(j));
							}
							else {
								replacement = "";
							}
							correctResult = correctResult.replace("$" + j, replacement);
						}
					}
				}

				if (!correctResult.toLowerCase().equals(s.toLowerCase())) {
					int distance = dl.execute(correctResult.toLowerCase(), s.toLowerCase());
					resultsByDistance.put(distance, (resultsByDistance.containsKey(distance)?resultsByDistance.get(distance):0) + 1);
					resultsMeanSize.put(distance, (resultsMeanSize.containsKey(distance)?resultsMeanSize.get(distance):0) + correctResult.length());

					if (distance == 5) {
						System.out.println(i + " (" + splitted[0] + ") " + s + " -> " + correctResult + " (" + distance + ")");
					}
				}
			}

		}

		br.close();

		System.out.println("RESULTS: ");
		int total = 0;
		for (Integer dist : resultsByDistance.keySet()) {
			total += resultsByDistance.get(dist);
		}
		for (Integer dist : resultsByDistance.keySet()) {
			System.out.println("Distance " + dist + ": " + resultsByDistance.get(dist) + " " + (float)resultsByDistance.get(dist)*100/total + "%");
			System.out.println("\t-> Mean size: " + (float)resultsMeanSize.get(dist) / resultsByDistance.get(dist));
		}
		System.out.println("TOTAL: " + total);

	}

}
